const http = require('http');

let users = [
	{
		"firstName": "Mary Jane",
		"lastName": "Dela Cruz",
		"mobileNo": "09123456789",
		"email": "mjdelacruz@mail.com",
		"password": 123
	},
	{
		"firstName": "John",
		"lastName": "Doe",
		"mobileNo": "09123456789",
		"email": "jdoe@mail.com",
		"password": 123
	}
]

http.createServer(function (req, res){
	if(req.url == "/profile" && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.write(JSON.stringify(users));
		res.end();
	}

	if(req.url == "/register" && req.method == "POST"){
		let register = '';
		req.on('data', function(data){
			register += data;
		})

		req.on('end', function(){
			console.log(typeof register)
			register = JSON.parse(register);
			let addUser = {
				"firstName": register.firstName,
				"lastName": register.lastName,
				"mobileNo": register.mobileNo,
				"email": register.email,
				"password": register.password
			}

			users.push(addUser)
			console.log(users);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(addUser));
			res.end();
		})
	}
}).listen(4000);

console.log('Server is running at localhost: 4000');

//test
/*			{
				"firstName": "Joey",
				"lastName": "Cabal",
				"mobileNo": "09123456789",
				"email": "joey@mail.com",
				"password": "password"
			}*/
